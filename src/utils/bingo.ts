import {BingoBoardData, BingoCellData} from '../types/bingo';
import phrases from '../constants/phrases';

// Function to shuffle the phrases array
export function shuffleArray<T>(array: T[]): T[] {
    const shuffledArray = [...array];

    for (let i = shuffledArray.length - 1; i > 0; i--) {
        const randomIndex = Math.floor(Math.random() * (i + 1));
        [shuffledArray[i], shuffledArray[randomIndex]] = [shuffledArray[randomIndex], shuffledArray[i]];
    }

    return shuffledArray;
}

export function generateBingoBoard(size: number): BingoBoardData {
    if (size < 1) {
        throw new Error("Invalid board size");
    }

    const middleIndex = Math.floor(size / 2);
    let id = 0;
    const phrasesOne = shuffleArray(phrases).slice(0, size * size - 1); // Exclude one phrase for the middle cell

    const boardData: BingoBoardData = [];

    for (let i = 0; i < size; i++) {
        const row: BingoCellData[] = [];
        for (let j = 0; j < size; j++) {
            const isMiddleCell = i === middleIndex && j === middleIndex;
            const cell: BingoCellData = {
                id: id++,
                phrase: isMiddleCell ? "CONF CALL 😆BINGO" : phrasesOne.shift() || "",
                marked: isMiddleCell,
            };
            row.push(cell);
        }
        boardData.push(row);
    }

    return boardData;
}
