export type BingoCellData = {
    id: number;
    phrase: string;
    marked: boolean;
};

export type BingoBoardData = BingoCellData[][];
