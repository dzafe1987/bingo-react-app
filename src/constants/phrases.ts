const phrases: string[] = [
    "(child noises in the background)",
    "Hello, hello?",
    "i need to jump in another call",
    "can everyone go on mute",
    "you have a bad connection",
    "(load painful echo feedback)",
    "Next slide, please.",
    "can we take this offline?",
    "is on ____ the call?",
    "Could you share this slides afterwards?",
    "can somebody grant presenter rights?",
    "can You email that to everyone?",
    "sorry, I had problems logging in",
    "(animal noises in the background)",
    "sorry, i didnt found the conference id",
    "i was having connection issues",
    "break?",
    "who just joined?",
    "sorrY, something with my calendar",
    "do you see my screen?",
    "lets wait for ____!",
    "You will send the minutes?",
    "sorry, i was on mute.",
    "can you repeat, please?"
];

export default phrases;
