import React from 'react';
import BingoCell from './BingoCell';
import './BingoBoard.css';
import {BingoBoardData, BingoCellData} from "../../types/bingo";

interface BingoBoardProps {
    boardData: BingoBoardData;
    setBoardData: (boardData: BingoBoardData) => void;
}

const BingoBoard: React.FC<BingoBoardProps> = ({ boardData, setBoardData }) => {
    const handleClick = (cell: BingoCellData) => {
        if (cell.id === 12) return;

        if (!cell.marked) {
            const updatedData = boardData.map(row =>
                row.map(currCell => (currCell.id === cell.id ? { ...currCell, marked: true } : currCell))
            );
            setBoardData(updatedData);
        } else{
            const updatedData = boardData.map(row =>
                row.map(currCell => (currCell.id === cell.id ? { ...currCell, marked: false } : currCell))
            );
            setBoardData(updatedData);
        }

    };


    return (
        <div className="bingo-board">
            {boardData.map((row, rowIndex) => (
                <div className="bingo-row" key={rowIndex}>
                    {row.map(cell => (
                        <BingoCell key={cell.id} cellData={cell} isMiddleCell={cell.id === 12} handleClick={handleClick} />
                    ))}
                </div>
            ))}
        </div>
    );
};

export default BingoBoard;
