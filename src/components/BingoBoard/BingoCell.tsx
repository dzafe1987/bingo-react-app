import React from 'react';
import { BingoCellData } from '../../types/bingo';
import './BingoCell.css';

interface BingoCellProps {
    cellData: BingoCellData;
    handleClick: (cell: BingoCellData) => void;
    isMiddleCell: boolean;
}

const BingoCell: React.FC<BingoCellProps> = ({ cellData, handleClick, isMiddleCell }) => {
    const handleClickCell = () => {
        if (isMiddleCell) return; // Ignore click on the middle cell
        handleClick(cellData);
    };

    return (
        <div
            className={`bingo-cell ${cellData.marked ? 'marked' : ''} ${isMiddleCell ? 'middle' : ''}`}
            onClick={handleClickCell}>
            <span>
                {cellData.phrase}
            </span>
        </div>
    );
};

export default BingoCell;
