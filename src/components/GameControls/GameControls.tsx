import React from 'react';
import './GameControls.css';

interface GameControlsProps {
    handleResetBoard: () => void;
}

const GameControls: React.FC<GameControlsProps> = ({handleResetBoard}) => {
    return (
        <div className="game-controls">
            <button className="reset-button" onClick={handleResetBoard}>
                Reset
            </button>
        </div>
    );
};

export default GameControls;
