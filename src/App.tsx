import React, {useState, useEffect} from 'react';
import BingoBoard from './components/BingoBoard/BingoBoard';
import GameControls from './components/GameControls/GameControls';
import {generateBingoBoard} from './utils/bingo';
import Confetti from 'react-confetti';
import {usePrevious} from "./utils/usePrevious";

const App: React.FC = () => {
    const boardSize = 5; // Adjust the board size as needed
    const initialBoardData = generateBingoBoard(boardSize);
    const [boardData, setBoardData] = useState(initialBoardData);
    const [winningRows, setWinningRows] = useState<number[]>([]);
    const [winningColumns, setWinningColumns] = useState<number[]>([]);
    const [winningDiagonals, setWinningDiagonals] = useState<number[]>([]);
    const [isCelebrating, setIsCelebrating] = useState(false);
    const prevAmount = usePrevious({winningRows, winningColumns, winningDiagonals});


    const handleResetBoard = () => {
        const newBoardData = generateBingoBoard(boardSize);
        setBoardData(newBoardData);
        setWinningRows([]);
        setWinningColumns([]);
        setWinningDiagonals([]);
    };

    useEffect(() => {
        checkWinCondition();
    }, [boardData]);

    const checkWinCondition = () => {
        let winningRows: number[] = [];
        let winningColumns: number[] = [];
        let winningDiagonals: number[] = [];

        // Check rows
        for (let i = 0; i < boardSize; i++) {
            let hasWonRow = true;
            for (let j = 0; j < boardSize; j++) {
                if (!boardData[i][j].marked) {
                    hasWonRow = false;
                    break;
                }
            }
            if (hasWonRow) {
                winningRows.push(i);
            }
        }

        // Check columns
        for (let i = 0; i < boardSize; i++) {
            let hasWonColumn = true;
            for (let j = 0; j < boardSize; j++) {
                if (!boardData[j][i].marked) {
                    hasWonColumn = false;
                    break;
                }
            }
            if (hasWonColumn) {
                winningColumns.push(i);
            }
        }
        // Check diagonals
        let hasWonDiagonal1 = true;
        let hasWonDiagonal2 = true;
        for (let i = 0; i < boardSize; i++) {
            if (!boardData[i][i].marked) {
                hasWonDiagonal1 = false;
            }
            if (!boardData[i][boardSize - 1 - i].marked) {
                hasWonDiagonal2 = false;
            }
        }
        if (hasWonDiagonal1) {
            winningDiagonals.push(1);
        }
        if (hasWonDiagonal2) {
            winningDiagonals.push(2);
        }

        setWinningRows(winningRows);
        setWinningColumns(winningColumns);
        setWinningDiagonals(winningDiagonals);
    };

    useEffect(() => {
        if (!prevAmount?.winningColumns && !prevAmount?.winningColumns && !prevAmount?.winningDiagonals)
            return
        if ((prevAmount.winningRows.length !== winningRows.length) && (prevAmount.winningRows.length < winningRows.length)) {
            setIsCelebrating(true)
        }
        if ((prevAmount.winningColumns.length !== winningColumns.length) && (prevAmount.winningColumns.length < winningColumns.length)) {
            setIsCelebrating(true)
        }

        if ((prevAmount.winningDiagonals.length !== winningDiagonals.length) && (prevAmount.winningDiagonals.length < winningDiagonals.length)) {
            setIsCelebrating(true)
        }

        const celebrationTimeout = setTimeout(() => {
            setIsCelebrating(false);
        }, 2000);

        // Clean up the timeout when the component unmounts or winning rows/columns change
        return () => clearTimeout(celebrationTimeout);

    }, [winningRows, winningColumns]);


    return (
        <div>
            {isCelebrating && (
                <Confetti
                    tweenDuration={2000}
                    recycle={false}
                    numberOfPieces={500}
                    gravity={0.4}
                    width={window.outerWidth}
                    height={window.outerHeight}
                />
            )}
            <GameControls handleResetBoard={handleResetBoard}/>
            <BingoBoard boardData={boardData} setBoardData={setBoardData}/>
        </div>
    );
};

export default App;
