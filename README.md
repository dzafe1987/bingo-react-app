Bingo React App

This is a simple React application for playing Bingo. The app allows users to mark cells as they are called out and celebrates the victory with confetti animation.

Features:
- Play Bingo by marking cells
- Responsive design for desktop and mobile devices
- Celebrate victory with confetti animation

Installation:
1. Clone the repository:
   git clone https://github.com/dzafe1987/bingo-react-app.git

2. Navigate to the project directory:
   cd bingo-react-app

3. Install the dependencies:
   npm install

Usage:
1. Start the development server:
   npm start

2. Open the app in your browser:
   http://localhost:3000

Dependencies:
- React
- React Confetti (v6.1.0)

Credits:
- The confetti animation is implemented using the React Confetti library. You can find more information about it here: [React Confetti](https://github.com/alampros/react-confetti)
